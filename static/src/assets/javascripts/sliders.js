class Slider {
    constructor() {
        this.sliderWide = $(".js-slider-wide");
        this.sliderFade = $(".js-slider-fade");
        this.sliderRegular = $(".js-slider-regular");

        this.sliderWideDots = $(".js-slider-wide-dots");
        this.sliderFadeDots = $(".js-slider-fade-dots");

        this.arrowLeft = $(".js-arrow-left");
        this.arrowRight = $(".js-arrow-right");
    }

    events(init = false) {
        let that = this;

        if (init) {

            /** SLIDER WIDE **/

            this.sliderWide.slick({
                dots: false,
                arrows: false,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: false,
                centerMode: true,
                variableWidth: true,
                rows: 0,
            }).find(".card").each(function (i) {
                let title = $(this).attr("data-title");
                if (i === 0) {
                    $("<li><a data-title='" + title + "' class='active' href='" + i + "'>" + title + "</a></li>").appendTo(that.sliderWideDots);
                } else {
                    $("<li><a data-title='" + title + "' href='" + i + "'>" + title + "</a></li>").appendTo(that.sliderWideDots);
                }
            });
            //     .on("setPosition", function (event, slick) {
            //     slick.$slides.css('height', slick.$slideTrack.height() + 'px');
            // })

            this.sliderWide.on("afterChange", function () {

                let currentSlide = $(".slick-current").attr("data-title");
                console.log(currentSlide);

                that.sliderWideDots.find("a").each(function () {
                    if ($(this).attr("data-title") === currentSlide) {
                        that.sliderWideDots.find("a").removeClass("active");
                        $(this).addClass("active");
                    }
                });

                let length = that.sliderWide.find(".card").length;

                if (parseInt(that.sliderWide.find(".slick-current").attr("data-slick-index")) === 0) {
                    $("[data-slide='js-slider-wide'].js-arrow-left").css("opacity", ".5");
                } else {
                    $("[data-slide='js-slider-wide'].js-arrow-left").css("opacity", "1");
                }

                if (parseInt(that.sliderWide.find(".slick-current").attr("data-slick-index")) === length - 1) {
                    $("[data-slide='js-slider-wide'].js-arrow-right").css("opacity", ".5");
                } else {
                    $("[data-slide='js-slider-wide'].js-arrow-right").css("opacity", "1");
                }

            });

            this.sliderWideDots.find("a").click(function (e) {
                e.preventDefault();

                that.sliderWideDots.find("a").removeClass("active");
                $(this).addClass("active");

                let index = $(this).attr("href");
                that.sliderWide.slick("slickGoTo", parseInt(index));

            });

            /** SLIDER FADE **/

            this.sliderFade.slick({
                dots: false,
                arrows: false,
                infinite: false,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                rows: 0
            }).find(".card").each(function (i) {
                let title = $(this).attr("data-title");
                if (i === 0) {
                    $("<li><a class='active' href='" + i + "'>" + title + "</a></li>").appendTo(that.sliderFadeDots);
                } else {
                    $("<li><a href='" + i + "'>" + title + "</a></li>").appendTo(that.sliderFadeDots);
                }
                $("<span class='pointer'></span>").appendTo($(this));
            });

            this.sliderFadeDots.find("a").click(function (e) {
                e.preventDefault();

                that.sliderFadeDots.find("a").removeClass("active");
                $(this).addClass("active");

                let index = $(this).attr("href");
                that.sliderFade.slick("slickGoTo", parseInt(index));

                let offsetTop = $(this).position().top;
                $(".pointer").css("top", offsetTop - 14);
            });

            this.sliderFade.on("afterChange", function () {

                let length = that.sliderFade.find(".card").length;

                if (parseInt(that.sliderFade.find(".slick-current").attr("data-slick-index")) === 0) {
                    $("[data-slide='js-slider-fade'].js-arrow-left").css("opacity", ".5");
                } else {
                    $("[data-slide='js-slider-fade'].js-arrow-left").css("opacity", "1");
                }

                if (parseInt(that.sliderFade.find(".slick-current").attr("data-slick-index")) === length - 1) {
                    $("[data-slide='js-slider-fade'].js-arrow-right").css("opacity", ".5");
                } else {
                    $("[data-slide='js-slider-fade'].js-arrow-right").css("opacity", "1");
                }
            });


            /** SLIDER REGULAR **/

            this.sliderRegular.slick({
                dots: false,
                arrows: false,
                speed: 500,
                slidesToShow: 5,
                slidesToScroll: 1,
                autoplay: true,
                rows: 0,
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                        }
                    }
                ]
            });

            /** ARROWS **/

            this.arrowLeft.on("click", function () {
                let slide = $(this).attr("data-slide");
                $("." + slide).slick("slickPrev");
            });

            this.arrowRight.on("click", function () {
                let slide = $(this).attr("data-slide");
                $("." + slide).slick("slickNext");
            });

        }
    }
}