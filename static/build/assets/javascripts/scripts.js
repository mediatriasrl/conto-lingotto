let cookie = document.cookie;

if (cookie) {
    $(".js-cookie-bar").addClass("hide");
} else {
    $(".js-cookie-bar").removeClass("hide");
}

$(".js-btn-mobile").on("click", function () {
    $(this).toggleClass("open");
    $("body").toggleClass("fixed");
    $(".js-menu-mobile").toggleClass("open");
});

$(".js-collapse-header").on("click", function () {

    let $this = $(this);
    let $parent = $this.parents().closest(".js-collapse");

    if ($parent.hasClass("open")) {

        $this.next(".js-collapse-body").slideUp();
        $parent.removeClass("open");

    } else {

        $(".js-collapse").removeClass("open");

        $(".js-collapse-body").slideUp();
        $this.next(".js-collapse-body").slideDown();

        $parent.addClass("open");

    }

});

$(".js-cookie-button").on("click", function () {
    $(".js-cookie-bar").fadeOut();
    document.cookie = "cookiebar=hide";
});
