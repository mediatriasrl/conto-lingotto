$(window).on("load resize", function(){

    if ($(window).width() > 991) {

        let controller = new ScrollMagic.Controller();

        let scene = new ScrollMagic.Scene({
            duration: 600,
            offset: 300,
        }).setTween(TweenMax.to("#pin1", .1, {x: "-50%", y: 510}));

        let scene2 = new ScrollMagic.Scene({
            offset: 490
        }).setTween(TweenMax.to("#pin2", 1, {x: 350, y: 0}));

        let scene3 = new ScrollMagic.Scene({
            offset: 1070
        }).setTween(TweenMax.to("#pin3", 1, {rotation: 0, transformOrigin: "center"}));

        let scene4 = new ScrollMagic.Scene({
            offset: 1070
        }).setTween(TweenMax.to("#pin4", 1, {x: 0, y: 0}));

        let scene5 = new ScrollMagic.Scene({
            offset: 3270
        }).setTween(TweenMax.to("#pin5", 1, {x: -160, y: 0}));

        let scene6 = new ScrollMagic.Scene({
            offset: 3270
        }).setTween(TweenMax.to("#pin6", 2, {x: "100%", y: 190}));

        let scene7 = new ScrollMagic.Scene({
            offset: 3270
        }).setTween(TweenMax.to("#pin7", 1, {width: "100%"}));

        let scene8 = new ScrollMagic.Scene({
            offset: 5250
        }).setTween(TweenMax.to("#pin8", 1, {x: 0, y: "-70%"}));

        let scene9 = new ScrollMagic.Scene({
            offset: 5250
        }).setTween(TweenMax.to("#pin9", 1, {x: "50%", y: "-30%"}));

        let scene10 = new ScrollMagic.Scene({
            offset: 5250
        }).setTween(TweenMax.to("#pin10", 1, {x: 0, y: "20%"}));

        controller.addScene([
            scene,
            scene2,
            scene3,
            scene4,
            scene5,
            scene6,
            scene7,
            scene8,
            scene9,
            scene10
        ]);

    }

});