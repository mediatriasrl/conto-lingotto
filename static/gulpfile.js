var gulp = require('gulp'),
  browserSync = require('browser-sync').create(),
  nunjucksRender = require('gulp-nunjucks-render'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  minify = require('gulp-minify');
  sourcemaps = require('gulp-sourcemaps'),
  cleanCSS = require('gulp-clean-css'),
  imagemin = require('gulp-imagemin'),
  concat = require('gulp-concat'),
  concatCss = require('gulp-concat-css'),
  htmlmin = require('gulp-htmlmin'),
  clean = require('gulp-clean'),
  data = require('gulp-data'),
  cssbeautify = require('gulp-cssbeautify'),
  babel = require('gulp-babel'),
  log = require('fancy-log'),
  fs = require('fs');

gulp.task('clean-fonts', function () {
  return gulp.src(['./dist/assets/fonts', './build/assets/fonts'], {read: false})
    .pipe(clean());
});

gulp.task('clean-images', function () {
  return gulp.src(['./dist/assets/images', './build/assets/images'], {read: false})
    .pipe(clean());
});

gulp.task('clean-icons', function () {
  return gulp.src(['./dist/assets/icons', './build/assets/icons'], {read: false})
    .pipe(clean());
});

gulp.task('clean-stylesheets', function () {
  return gulp.src(['./dist/assets/stylesheets', './build/assets/stylesheets'], {read: false})
    .pipe(clean());
});

gulp.task('clean-javascripts', function () {
  return gulp.src(['./dist/assets/javascripts', './build/assets/javascripts'], {read: false})
    .pipe(clean());
});

gulp.task('sass', ['clean-stylesheets'], function () {
  return gulp.src('./src/assets/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(cssbeautify({
      indent: '  ',
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./build/assets/stylesheets'))
    .pipe(cleanCSS())
    .pipe(gulp.dest('./dist/assets/stylesheets'))
    .pipe(browserSync.stream());
});

gulp.task('compressJs', ['vendorsScripts'], function () {
  return gulp.src('./src/assets/javascripts/*.js')
    .pipe(gulp.dest('./build/assets/javascripts'))
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(minify({
      ext: {
        min: '.min.js'
      },
      noSource: true
    }))
    .on('end', function(){ log('compressJs done!'); })
    .pipe(gulp.dest('./dist/assets/javascripts'))
    .pipe(browserSync.stream());
});

gulp.task('vendorsScripts', ['clean-javascripts'], function () {
  return gulp.src(['node_modules/jquery/dist/jquery.min.js', 'node_modules/scrollmagic/scrollmagic/minified/ScrollMagic.min.js', 'node_modules/scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min.js', 'node_modules/scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js', 'node_modules/bootstrap/dist/js/bootstrap.js', 'node_modules/bootstrap/dist/js/bootstrap.bundle.js'])
    .pipe(concat('vendors.js'))
    .pipe(gulp.dest('./build/assets/javascripts'))
    .pipe(minify({
      ext: {
        min: '.min.js'
      },
      noSource: true
    }))
    .pipe(gulp.dest('./dist/assets/javascripts'));
});

gulp.task('vendorStyles', function () {
  return gulp.src('')
    .pipe(concatCss("vendors.css"))
    .pipe(gulp.dest('./dist/assets/stylesheets/'))
    .pipe(gulp.dest('./build/assets/stylesheets/'));
});

gulp.task('compressImages', ['clean-images'], function () {
  return gulp.src('./src/assets/images/**')
    .pipe(gulp.dest('./build/assets/images'))
    .pipe(imagemin({
      progressive: true,
      optimizationLevel: 3
    }))
    .pipe(gulp.dest('./dist/assets/images'))
});

gulp.task('copyFontawesome', function() {
  return gulp.src('./node_modules/@fortawesome/fontawesome-pro/webfonts/*')
    .pipe(gulp.dest('./dist/assets/fonts'))
    .pipe(gulp.dest('./build/assets/fonts'))
});

gulp.task('copyFonts', ['clean-fonts', 'copyFontawesome'], function () {
  return gulp.src('./src/assets/fonts/**')
    .pipe(gulp.dest('./dist/assets/fonts'))
    .pipe(gulp.dest('./build/assets/fonts'))
});

gulp.task('copyIcons', ['clean-icons'], function () {
  return gulp.src('./src/assets/icons/**')
    .pipe(gulp.dest('./dist/assets/icons'))
    .pipe(gulp.dest('./build/assets/icons'))
});

gulp.task('nunjucks', function () {
  var dataFile = './src/assets/data/pages.json';
  return gulp.src('./src/pages/**/*.+(html|njk)')
    .pipe(data(function () {
      return JSON.parse(fs.readFileSync(dataFile));
    }))
    .pipe(nunjucksRender({
      path: ['./src/templates'],
      envOptions: {autoescape: false},
    }))
    .pipe(gulp.dest('build'))
    .pipe(htmlmin(
      {
        collapseWhitespace: true,
        removeComments: true
      }))
    .pipe(gulp.dest('dist'))
});

// Static Server + watching scss/html files.
gulp.task('serve', ['sass', 'nunjucks-html-watch', 'compressJs', 'copy-config'], function () {
  gulp.watch('./src/assets/scss/**/*.scss', ['sass']);
  gulp.watch('./src/**/*.njk', ['nunjucks-html-watch']);
  gulp.watch('./src/assets/javascripts/*.js', ['compressJs']);
  browserSync.init({
    server: {
      baseDir: './build',
      index: "index.html",
      serveStaticOptions: {
        extensions: ['html']
      }
    }
  });
});

// Create a task that ensures the `nunjucks` task is complete before reloading browsers.
gulp.task('nunjucks-html-watch', ['nunjucks'], function () {
  browserSync.reload();
});

gulp.task('copy-config', function () {
  gulp.src([
    'config/web.config'
  ])
    .pipe(gulp.dest('build'))
});

// Compile project.
gulp.task('build', ['nunjucks', 'sass', 'copyFonts', 'copyIcons', 'compressImages', 'compressJs']);

// Compile and start project.
gulp.task('default', ['build', 'serve']);

